﻿# ref: https://github.com/dotnet/dotnet-docker/blob/main/samples/aspnetapp/Dockerfile.alpine
FROM mcr.microsoft.com/dotnet/sdk:8.0@sha256:032381bcea86fa0a408af5df63a930f1ff5b03116c940a7cd744d3b648e66749 AS build

ENV WAIT_VERSION=2.12.0
# https://github.com/ufoscout/docker-compose-wait
ADD https://github.com/ufoscout/docker-compose-wait/releases/download/${WAIT_VERSION}/wait /wait
RUN chmod +x /wait

COPY ./src/PowderbookingApi/PowderbookingApi.csproj ./src/PowderbookingApi/
RUN dotnet restore src/PowderbookingApi/PowderbookingApi.csproj

COPY . .
RUN dotnet publish src/PowderbookingApi/PowderbookingApi.csproj -c Release -o /app --no-restore
RUN rm /app/*.Development.json

FROM mcr.microsoft.com/dotnet/aspnet:8.0-alpine@sha256:a0062262fd674573be2314890006acadfd233fd7fdf89b3a5f44694b7632b29e

WORKDIR /app
COPY --from=build /app .
COPY --from=build /wait /wait
COPY ./entrypoint.sh .

USER $APP_UID

ENTRYPOINT ["/app/entrypoint.sh"]
CMD dotnet PowderbookingApi.dll