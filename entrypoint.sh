#!/bin/sh

#
# Copyright (c) 2020. Michael Kemna.
#

set -e

export DOTNET_POSTGRESQL_HOST=${POSTGRESQL_HOST}
export DOTNET_POSTGRESQL_PORT=${POSTGRESQL_PORT}
export DOTNET_POSTGRESQL_DATABASE=${POSTGRESQL_DATABASE}
export DOTNET_POSTGRESQL_USER=${POSTGRESQL_USER}
export DOTNET_POSTGRESQL_PASSWORD=${POSTGRESQL_PASSWORD}

# https://github.com/ufoscout/docker-compose-wait
/wait

echo "Starting app from entrypoint.."

exec "$@"
