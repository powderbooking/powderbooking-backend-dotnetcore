using PowderbookingApi.Extensions;

namespace PowderbookingApi.UnitTests.Extensions;

public class StringExtensionsTests
{
    [Fact]
    public void ToSnakeCase()
    {
        // arrange
        var input = "UpperCase";

        // act
        var result = input.ToSnakeCase();

        // assert
        result.Should().Be("upper_case");
    }
}