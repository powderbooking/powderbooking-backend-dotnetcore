using System.Text.Json.Serialization;
using Microsoft.AspNetCore.Builder;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using PowderbookingApi.Extensions;
using PowderbookingApi.Helpers;

var builder = WebApplication.CreateBuilder(args);
IConfiguration config = builder.Configuration;


builder.Services.ConfigureServices(config);
builder.Services.AddHealthChecks();
builder.Services.AddControllers().AddJsonOptions(options =>
{
    options.JsonSerializerOptions.PropertyNamingPolicy = new SnakeCasePropertyNamingPolicy();
    options.JsonSerializerOptions.DefaultIgnoreCondition = JsonIgnoreCondition.WhenWritingNull;
});
builder.Services.ConfigureSwagger();

var app = builder.Build();

// using in production because it's behind a network anyway
app.UseSwagger();
app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "PowderbookingApi v1"));

// ref: https://stackoverflow.com/a/54153132
using (var Scope = app.Services.CreateScope())
{
    var context = Scope.ServiceProvider.GetService<PowderbookingContext>();
    // context.Database.EnsureCreated();
    context?.Database.Migrate();
}

app.UseMiddleware<ErrorHandlerMiddleware>();

// removed given we handle https outside of the application
// app.UseHttpsRedirection();

app.UseRouting();

// removed given we don't currently use authorization
// app.UseAuthorization();

app.MapControllers();
app.MapHealthChecks("/healthz");

app.Run();