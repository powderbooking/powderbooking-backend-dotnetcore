using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace PowderbookingApi.DTOs;

public class RankedBatchDto<T>
{
    /// <summary>
    ///     The batch with data
    /// </summary>
    /// <value>An array of the entity, ordered by rank</value>
    [Required]
    public required List<T> Batch { get; set; } = new();

    /// <summary>
    ///     The ranking ID of which this batch is part of
    /// </summary>
    /// <value>ranking ID</value>
    [Required]
    public required int RankingId { get; set; }

    /// <summary>
    ///     The rank of the first entity in the batch, if it's not the first
    /// </summary>
    /// <value>If not the first, the rank</value>
    public int? FirstRank { get; set; }

    /// <summary>
    ///     The rank of the last entity in the batch, if it's not the last
    /// </summary>
    /// <value>If not the last, the rank</value>
    public int? LastRank { get; set; }
}