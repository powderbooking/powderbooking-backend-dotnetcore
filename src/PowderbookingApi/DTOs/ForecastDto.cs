using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace PowderbookingApi.DTOs;

public class ForecastDto : ForecastWeekPostDto
{
    /// <summary>
    ///     The identifier of the entity
    /// </summary>
    /// <value>The identifier of the entity</value>
    [Required]
    public required int Id { get; set; }

    /// <summary>
    ///     The date time on which the entity was created in the database (UTC)
    /// </summary>
    /// <value>The date time on which the entity was created in the database</value>
    [Required]
    public required DateTime Created { get; set; }

    [Required] public required IEnumerable<ForecastDayDto> ForecastDays { get; set; }
}