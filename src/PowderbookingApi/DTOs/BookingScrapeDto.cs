using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace PowderbookingApi.DTOs;

public class BookingScrapeDto
{
    [Required] public required IEnumerable<BookingTargetsDto> Targets { get; set; }

    /// <summary>
    ///     The checkin date
    /// </summary>
    /// <value>The checkin date</value>
    [Required]
    public required DateOnly Checkin { get; set; }

    /// <summary>
    ///     The checkout date
    /// </summary>
    /// <value>The checkout date</value>
    [Required]
    public required DateOnly Checkout { get; set; }

    /// <summary>
    ///     The amount of adults
    /// </summary>
    [Required]
    public required int Adults { get; set; }

    /// <summary>
    ///     The amount of children
    /// </summary>
    [Required]
    public required int Children { get; set; }

    /// <summary>
    ///     The amount of rooms
    /// </summary>
    [Required]
    public required int Rooms { get; set; }
}

public class BookingTargetsDto
{
    /// <summary>
    ///     The identifier of the resort
    /// </summary>
    /// <value>The identifier of the resort</value>
    [Required]
    public required int Id { get; set; }

    /// <summary>
    ///     The Booking destination ID associated with the resort
    /// </summary>
    /// <value>The Booking destination ID</value>
    [Required]
    public required string BookingDestId { get; set; }
}