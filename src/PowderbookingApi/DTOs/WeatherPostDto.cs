using System;
using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;

namespace PowderbookingApi.DTOs;

public class WeatherPostDto
{
    /// <summary>
    ///     Foreign key to the related resort
    /// </summary>
    /// <value>Foreign key to the related resort</value>
    [Required]
    public required int ResortId { get; set; }

    /// <summary>
    ///     The datetime as given by the scraped api
    /// </summary>
    /// <value>The datetime as given by the scraped api</value>
    [Required]
    public required DateTime Dt { get; set; }

    /// <summary>
    ///     The temperature in Celsius
    /// </summary>
    /// <value>The temperature in Celsius</value>
    public double? TemperatureC { get; set; }

    /// <summary>
    ///     The wind speed in kilometres per hour
    /// </summary>
    /// <value>The wind speed in kilometres per hour</value>
    public double? WindSpeedKmh { get; set; }

    /// <summary>
    ///     The wind direction in degrees
    /// </summary>
    /// <value>The wind direction in degrees</value>
    public double? WindDirectionDeg { get; set; }

    /// <summary>
    ///     The visibility in kilometres
    /// </summary>
    /// <value>The visibility in kilometres</value>
    public double? VisibilityKm { get; set; }

    /// <summary>
    ///     The amount of clouds in percentage
    /// </summary>
    /// <value>The amount of clouds in percentage</value>
    public double? CloudsPct { get; set; }

    /// <summary>
    ///     The amount of snow in the last 3 hours in millimetres
    /// </summary>
    /// <value>The amount of snow in the last 3 hours in millimetres</value>
    [JsonPropertyName("snow_3h_mm")]
    public double? Snow3hMm { get; set; }

    /// <summary>
    ///     The amount of rain in the last 3 hours in millimetres
    /// </summary>
    /// <value>The amount of rain in the last 3 hours in millimetres</value>
    [JsonPropertyName("rain_3h_mm")]
    public double? Rain3hMm { get; set; }
}