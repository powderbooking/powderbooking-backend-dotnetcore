using System;
using System.ComponentModel.DataAnnotations;

namespace PowderbookingApi.DTOs;

public class ForecastDayPostDto
{
    /// <summary>
    ///     Foreign key to the related resort
    /// </summary>
    /// <value>Foreign key to the related resort</value>
    [Required]
    public required int ResortId { get; set; }

    /// <summary>
    ///     The date as given by the scraped api
    /// </summary>
    [Required]
    public required DateOnly Date { get; set; }

    /// <summary>
    ///     The predicted maximum temperature in Celsius
    /// </summary>
    public double? TemperatureMaxC { get; set; }

    /// <summary>
    ///     The predicted minimum temperature in Celsius
    /// </summary>
    public double? TemperatureMinC { get; set; }

    /// <summary>
    ///     The predicted amount of rain during the day in millimetres
    /// </summary>
    public double? RainTotalMm { get; set; }

    /// <summary>
    ///     The predicted amount of snow during the day in millimetres
    /// </summary>
    public double? SnowTotalMm { get; set; }

    /// <summary>
    ///     The predicted probability of precipitations during the day in percentage
    /// </summary>
    public double? ProbPrecipPct { get; set; }

    /// <summary>
    ///     The predicted maximum windspeed in km/h
    /// </summary>
    public double? WindSpeedMaxKmh { get; set; }

    /// <summary>
    ///     The predicted maximum windspeed of gusts in km/h
    /// </summary>
    public double? WindgstMaxKmh { get; set; }
}