using System;
using System.ComponentModel.DataAnnotations;

namespace PowderbookingApi.DTOs;

public class WeatherDto : WeatherPostDto
{
    /// <summary>
    ///     The identifier of the entity
    /// </summary>
    /// <value>The identifier of the entity</value>
    [Required]
    public required int Id { get; set; }

    /// <summary>
    ///     The date derived from dt
    /// </summary>
    /// <value>The date derived from dt</value>
    [Required]
    public required DateOnly Date { get; set; }

    /// <summary>
    ///     Ordinal timepoint of the day, categorized every 3 hours (range 0-7)
    /// </summary>
    /// <value>Ordinal timepoint of the day, categorized every 3 hours (range 0-7)</value>
    [Required]
    public required int Timepoint { get; set; }

    /// <summary>
    ///     The date time on which the entity was created in the database (UTC)
    /// </summary>
    /// <value>The date time on which the entity was created in the database</value>
    [Required]
    public required DateTime Created { get; set; }
}