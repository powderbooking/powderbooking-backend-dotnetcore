using System.ComponentModel.DataAnnotations;

namespace PowderbookingApi.DTOs;

/// <summary>
///     Resort
/// </summary>
public class ResortPostDto
{
    /// <summary>
    ///     The continent where the resort is located
    /// </summary>
    /// <value>The continent where the resort is located</value>
    [Required]
    public required string Continent { get; set; }

    /// <summary>
    ///     The country where the resort is located
    /// </summary>
    /// <value>The country where the resort is located</value>
    [Required]
    public required string Country { get; set; }

    /// <summary>
    ///     The village where the resort is located
    /// </summary>
    /// <value>The village where the resort is located</value>
    [Required]
    public required string Village { get; set; }

    /// <summary>
    ///     The latitudinal coordinate of the geolocation of the resort
    /// </summary>
    /// <value>The latitudinal coordinate of the geolocation of the resort</value>
    [Required]
    public required double Lat { get; set; }

    /// <summary>
    ///     The longitudinal coordinate of the geolocation of the resort
    /// </summary>
    /// <value>The longitudinal coordinate of the geolocation of the resort</value>
    [Required]
    public required double Lng { get; set; }

    /// <summary>
    ///     The destination ID of Booking that is associated to this resort
    /// </summary>
    /// <value>The destination ID of Booking that is associated to this resort</value>
    public string? BookingDestId { get; set; }

    /// <summary>
    ///     The lowest altitude of the resort (in metres)
    /// </summary>
    /// <value>The lowest altitude of the resort (in metres)</value>
    public int? AltitudeMinM { get; set; }

    /// <summary>
    ///     The highest altitude of the resort (in metres)
    /// </summary>
    /// <value>The highest altitude of the resort (in metres)</value>
    public int? AltitudeMaxM { get; set; }

    /// <summary>
    ///     The amount of lifts in the resort
    /// </summary>
    /// <value>The amount of lifts in the resort</value>
    public int? Lifts { get; set; }

    /// <summary>
    ///     The length of all slopes in the resort (in kilometres)
    /// </summary>
    /// <value>The length of all slopes in the resort (in kilometres)</value>
    public int? SlopesTotalKm { get; set; }

    /// <summary>
    ///     The length of blue slopes in the resort (in kilometres)
    /// </summary>
    /// <value>The length of blue slopes in the resort (in kilometres)</value>
    public int? SlopesBlueKm { get; set; }

    /// <summary>
    ///     The length of red slopes in the resort (in kilometres)
    /// </summary>
    /// <value>The length of red slopes in the resort (in kilometres)</value>
    public int? SlopesRedKm { get; set; }

    /// <summary>
    ///     The length of black slopes in the resort (in kilometres)
    /// </summary>
    /// <value>The length of black slopes in the resort (in kilometres)</value>
    public int? SlopesBlackKm { get; set; }
}