using System;
using System.ComponentModel.DataAnnotations;

namespace PowderbookingApi.DTOs;

public class AvailabilityDto : AvailabilityPostDto
{
    /// <summary>
    ///     The identifier of the entity
    /// </summary>
    /// <value>The identifier of the entity</value>
    [Required]
    public int Id { get; set; }

    /// <summary>
    ///     The date time on which the entity was created in the database (UTC)
    /// </summary>
    /// <value>The date time on which the entity was created in the database</value>
    [Required]
    public DateTime Created { get; set; }
}