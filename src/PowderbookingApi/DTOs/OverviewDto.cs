using System.ComponentModel.DataAnnotations;

namespace PowderbookingApi.DTOs;

public class OverviewDto : ForecastWeekBaseDto
{
    /// <summary>
    ///     The village where the resort is located
    /// </summary>
    /// <value>The village where the resort is located</value>
    [Required]
    public required string Village { get; set; }

    /// <summary>
    ///     The latitudinal coordinate of the geolocation of the resort
    /// </summary>
    /// <value>The latitudinal coordinate of the geolocation of the resort</value>
    [Required]
    public required double Lat { get; set; }

    /// <summary>
    ///     The longitudinal coordinate of the geolocation of the resort
    /// </summary>
    /// <value>The longitudinal coordinate of the geolocation of the resort</value>
    [Required]
    public required double Lng { get; set; }
}