using System.ComponentModel.DataAnnotations;

namespace PowderbookingApi.DTOs;

/// <summary>
///     Resort
/// </summary>
public class ResortDto : ResortPostDto
{
    /// <summary>
    ///     The identifier of the resort
    /// </summary>
    /// <value>The identifier of the resort</value>
    [Required]
    public int Id { get; set; }
}