using System.ComponentModel.DataAnnotations;

namespace PowderbookingApi.DTOs;

public class ScrapeDto
{
    /// <summary>
    ///     The identifier of the resort
    /// </summary>
    /// <value>The identifier of the resort</value>
    [Required]
    public required int Id { get; set; }

    /// <summary>
    ///     The latitudinal coordinate of the geolocation of the resort
    /// </summary>
    /// <value>The latitudinal coordinate of the geolocation of the resort</value>
    [Required]
    public required double Lat { get; set; }

    /// <summary>
    ///     The longitudinal coordinate of the geolocation of the resort
    /// </summary>
    /// <value>The longitudinal coordinate of the geolocation of the resort</value>
    [Required]
    public required double Lng { get; set; }
}