using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace PowderbookingApi.DTOs;

public class ForecastPostDto : ForecastWeekPostDto
{
    [Required] public required IEnumerable<ForecastDayPostDto> ForecastDays { get; set; }
}