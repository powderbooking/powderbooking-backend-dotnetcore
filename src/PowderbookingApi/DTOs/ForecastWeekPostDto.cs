using System;
using System.ComponentModel.DataAnnotations;

namespace PowderbookingApi.DTOs;

public class ForecastWeekBaseDto
{
    /// <summary>
    ///     The identifier of the resort
    /// </summary>
    /// <value>The identifier of the resort</value>
    [Required]
    public int ResortId { get; set; }

    [Required] public double RainTotalMm { get; set; }

    [Required] public double SnowTotalMm { get; set; }
}

public class ForecastWeekPostDto : ForecastWeekBaseDto
{
    /// <summary>
    ///     The date given by the API
    /// </summary>
    /// <value>The date derived from dt</value>
    [Required]
    public DateOnly Date { get; set; }
}