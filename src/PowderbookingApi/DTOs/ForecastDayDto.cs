using System;
using System.ComponentModel.DataAnnotations;

namespace PowderbookingApi.DTOs;

public class ForecastDayDto : ForecastDayPostDto
{
    /// <summary>
    ///     The identifier of the entity
    /// </summary>
    /// <value>The identifier of the entity</value>
    [Required]
    public required int Id { get; set; }

    /// <summary>
    ///     Ordinal timepoint of the week, categorized every day (range 0-6)
    ///     0 - the forecast is given for the same day
    ///     6 - the forecast is given for next week
    /// </summary>
    [Required]
    public required int Timepoint { get; set; }

    /// <summary>
    ///     The date time on which the entity was created in the database (UTC)
    /// </summary>
    /// <value>The date time on which the entity was created in the database</value>
    [Required]
    public required DateTime Created { get; set; }
}