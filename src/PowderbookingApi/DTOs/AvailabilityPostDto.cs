using System;
using System.ComponentModel.DataAnnotations;

namespace PowderbookingApi.DTOs;

public class AvailabilityPostDto
{
    /// <summary>
    ///     Foreign key to the related resort
    /// </summary>
    /// <value>Foreign key to the related resort</value>
    [Required]
    public required int ResortId { get; set; }

    /// <summary>
    ///     The name of the hotel
    /// </summary>
    [Required]
    public required string Name { get; set; }

    /// <summary>
    ///     The URL to visit the hotel
    /// </summary>
    [Required]
    public required string Url { get; set; }

    /// <summary>
    ///     The rating of the hotel at the time of scraping
    /// </summary>
    public string? Rating { get; set; }

    /// <summary>
    ///     The checkin date
    /// </summary>
    [Required]
    public required DateOnly Checkin { get; set; }

    /// <summary>
    ///     The checkout date
    /// </summary>
    [Required]
    public required DateOnly Checkout { get; set; }

    /// <summary>
    ///     The price to book at the time of scraping (in euro CENTS)
    /// </summary>
    [Required]
    public required int Price { get; set; }
}