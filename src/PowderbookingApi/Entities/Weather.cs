﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

namespace PowderbookingApi.Entities;

[Table("weather")]
[Index(nameof(Date), nameof(Timepoint), nameof(ResortId), Name = "weather_date_timepoint_resort_id_key",
    IsUnique = true)]
public class Weather
{
    [Key]
    [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
    [Column("id")]
    public int? Id { get; set; }

    // database generated
    [Column("created", TypeName = "timestamp with time zone")]
    public DateTime? Created { get; set; }

    [Column("resort_id")] public required int ResortId { get; set; }

    // as given by the API (unix UTC)
    // ref: https://openweathermap.org/current#parameter
    [Column("dt", TypeName = "timestamp with time zone")]
    public required DateTime Dt { get; set; }

    [Column("date")] public required DateOnly Date { get; set; }

    [Column("timepoint")] public required int Timepoint { get; set; }

    [Column("temperature_c")] public double? TemperatureC { get; set; }

    [Column("wind_speed_kmh")] public double? WindSpeedKmh { get; set; }

    [Column("wind_direction_deg")] public double? WindDirectionDeg { get; set; }

    [Column("visibility_km")] public double? VisibilityKm { get; set; }

    [Column("clouds_pct")] public double? CloudsPct { get; set; }

    [Column("snow_3h_mm")] public double? Snow3hMm { get; set; }

    [Column("rain_3h_mm")] public double? Rain3hMm { get; set; }

    [ForeignKey("ResortId")]
    [InverseProperty("Weathers")]
    public virtual Resort? Resort { get; set; }
}