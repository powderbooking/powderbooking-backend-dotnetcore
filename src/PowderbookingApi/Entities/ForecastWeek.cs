﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

namespace PowderbookingApi.Entities;

[Table("forecast_week")]
[Index("Date", "ResortId", Name = "forecast_week_date_resort_id_key", IsUnique = true)]
public class ForecastWeek
{
    [Key]
    [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
    [Column("id")]
    public int Id { get; set; }

    [Column("created", TypeName = "timestamp with time zone")]
    public DateTime? Created { get; set; }

    [Column("resort_id")] public required int ResortId { get; set; }

    [Column("date")] public required DateOnly Date { get; set; }

    [Column("rain_total_mm")] public required double RainTotalMm { get; set; }

    [Column("snow_total_mm")] public required double SnowTotalMm { get; set; }

    [ForeignKey("ResortId")]
    [InverseProperty("ForecastWeeks")]
    public virtual Resort? Resort { get; set; }

    [InverseProperty("ForecastWeek")] public virtual ICollection<ForecastDay>? ForecastDays { get; set; }

    [InverseProperty("ForecastWeek")] public virtual ICollection<ResortRank>? ResortRanks { get; set; }
}