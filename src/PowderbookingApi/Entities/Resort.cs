﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

namespace PowderbookingApi.Entities;

[Table("resort")]
[Index("Village", "Country", Name = "resort_village_country_key", IsUnique = true)]
public class Resort
{
    /// <summary>
    ///     The identifier of the resort
    /// </summary>
    [Key]
    [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
    [Column("id")]
    public int Id { get; set; }

    /// <summary>
    ///     The continent where the resort is located
    /// </summary>
    [Column("continent", TypeName = "character varying")]
    public required string Continent { get; set; }

    /// <summary>
    ///     The country where the resort is located
    /// </summary>
    [Column("country", TypeName = "character varying")]
    public required string Country { get; set; }

    /// <summary>
    ///     The village where the resort is located
    /// </summary>
    [Column("village", TypeName = "character varying")]
    public required string Village { get; set; }

    /// <summary>
    ///     The latitudinal coordinate of the geolocation of the resort
    /// </summary>
    [Column("lat")]
    public required double Lat { get; set; }

    /// <summary>
    ///     The longitudinal coordinate of the geolocation of the resort
    /// </summary>
    [Column("lng")]
    public required double Lng { get; set; }

    /// <summary>
    ///     The destination ID of Booking that is associated to this resort
    /// </summary>
    [Column("booking_dest_id", TypeName = "character varying")]
    public string? BookingDestId { get; set; }

    /// <summary>
    ///     The lowest altitude of the resort (in metres)
    /// </summary>
    [Column("altitude_min_m")]
    public int? AltitudeMinM { get; set; }

    /// <summary>
    ///     The highest altitude of the resort (in metres)
    /// </summary>
    [Column("altitude_max_m")]
    public int? AltitudeMaxM { get; set; }

    /// <summary>
    ///     The amount of lifts in the resort
    /// </summary>
    [Column("lifts")]
    public int? Lifts { get; set; }

    /// <summary>
    ///     The length of all slopes in the resort (in kilometres)
    /// </summary>
    [Column("slopes_total_km")]
    public int? SlopesTotalKm { get; set; }

    /// <summary>
    ///     The length of blue slopes in the resort (in kilometres)
    /// </summary>
    [Column("slopes_blue_km")]
    public int? SlopesBlueKm { get; set; }

    /// <summary>
    ///     The length of red slopes in the resort (in kilometres)
    /// </summary>
    [Column("slopes_red_km")]
    public int? SlopesRedKm { get; set; }

    /// <summary>
    ///     The length of black slopes in the resort (in kilometres)
    /// </summary>
    [Column("slopes_black_km")]
    public int? SlopesBlackKm { get; set; }

    [Column("last_scrape_forecast", TypeName = "timestamp with time zone")]
    public DateTime? LastScrapeForecast { get; set; }

    [Column("last_scrape_weather", TypeName = "timestamp with time zone")]
    public DateTime? LastScrapeWeather { get; set; }

    [Column("last_scrape_booking", TypeName = "timestamp with time zone")]
    public DateTime? LastScrapeBooking { get; set; }

    [InverseProperty("Resort")]
    public virtual ICollection<ForecastWeek> ForecastWeeks { get; } = new List<ForecastWeek>();

    [InverseProperty("Resort")] public virtual ICollection<ForecastDay> ForecastDays { get; } = new List<ForecastDay>();

    [InverseProperty("Resort")] public virtual ICollection<Weather> Weathers { get; } = new List<Weather>();

    [InverseProperty("Resort")]
    public virtual ICollection<Availability> Availabilities { get; } = new List<Availability>();

    [InverseProperty("Resort")] public virtual ICollection<ResortRank> ResortRanks { get; } = new List<ResortRank>();
}