﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace PowderbookingApi.Entities;

[Table("availability")]
public class Availability
{
    [Key]
    [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
    [Column("id")]
    public int Id { get; set; }

    // database generated
    [Column("created", TypeName = "timestamp with time zone")]
    public DateTime? Created { get; set; }

    [Column("resort_id")] public required int ResortId { get; set; }

    /// <summary>
    ///     The name of the hotel
    /// </summary>
    [Column("name")]
    public required string Name { get; set; }

    /// <summary>
    ///     The URL to visit the hotel
    /// </summary>
    [Column("url")]
    public required string Url { get; set; }

    [Column("rating")] public string? Rating { get; set; }

    [Column("checkin")] public required DateOnly Checkin { get; set; }

    [Column("checkout")] public required DateOnly Checkout { get; set; }

    /// <summary>
    ///     The price (in euro CENTS)
    /// </summary>
    [Column("price")]
    public required int Price { get; set; }

    [ForeignKey("ResortId")]
    [InverseProperty("Availabilities")]
    public virtual Resort? Resort { get; set; }
}