﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace PowderbookingApi.Entities;

[Table("ranking")]
public class Ranking
{
    [Key]
    [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
    [Column("id")]
    public int Id { get; set; }

    [Column("created", TypeName = "timestamp with time zone")]
    public DateTime? Created { get; set; }

    // programmatically maintained instead of relational because I'm the only one maintaining it
    // 1 = forecast
    // 2 = booking
    [Column("type")] public required int Type { get; set; }

    [Column("first_id")] public int FirstId { get; set; }

    [Column("last_id")] public int LastId { get; set; }

    [InverseProperty("Ranking")]
    public virtual IEnumerable<ResortRank> ResortRanks { get; set; } = new List<ResortRank>();
}