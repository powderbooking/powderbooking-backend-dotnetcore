﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

namespace PowderbookingApi.Entities;

[Table("resort_rank")]
[Index("ResortId", "RankingId", Name = "resort_ranking_key", IsUnique = true)]
public class ResortRank
{
    // the rank is implicit in the primary key
    [Key]
    [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
    [Column("id")]
    public int Id { get; set; }

    [Column("resort_id")] public required int ResortId { get; set; }

    [Column("ranking_id")] public required int RankingId { get; set; }

    // TODO: add uniqueness constraint
    // TODO: add forecast ID for even faster lookup

    [Column("forecast_week_id")] public required int ForecastWeekId { get; set; }

    [ForeignKey("ResortId")]
    [InverseProperty("ResortRanks")]
    public virtual Resort? Resort { get; set; }

    [ForeignKey("RankingId")]
    [InverseProperty("ResortRanks")]
    public virtual Ranking? Ranking { get; set; }

    [ForeignKey("ForecastWeekId")]
    [InverseProperty("ResortRanks")]
    public virtual ForecastWeek? ForecastWeek { get; set; }
}