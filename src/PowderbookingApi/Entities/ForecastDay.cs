﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

namespace PowderbookingApi.Entities;

[Table("forecast_day")]
[Index("Date", "Timepoint", "ResortId", Name = "forecast_day_date_timepoint_resort_id_key", IsUnique = true)]
public class ForecastDay
{
    [Key]
    [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
    [Column("id")]
    public int Id { get; set; }

    [Column("created", TypeName = "timestamp with time zone")]
    public DateTime? Created { get; set; }

    [Column("resort_id")] public required int ResortId { get; set; }

    [Column("forecast_week_id")] public int ForecastWeekId { get; set; }

    // the date as given by the API
    // ref: https://developer.weatherunlocked.com/documentation/localweather/resources#response-fields
    [Column("date")] public required DateOnly Date { get; set; }

    [Column("timepoint")] public required int Timepoint { get; set; }

    [Column("temperature_max_c")] public double? TemperatureMaxC { get; set; }

    [Column("temperature_min_c")] public double? TemperatureMinC { get; set; }

    [Column("rain_total_mm")] public double? RainTotalMm { get; set; }

    [Column("snow_total_mm")] public double? SnowTotalMm { get; set; }

    [Column("prob_precip_pct")] public double? ProbPrecipPct { get; set; }

    [Column("wind_speed_max_kmh")] public double? WindSpeedMaxKmh { get; set; }

    [Column("windgst_max_kmh")] public double? WindgstMaxKmh { get; set; }

    [ForeignKey("ResortId")]
    [InverseProperty("ForecastDays")]
    public virtual Resort? Resort { get; set; }

    [ForeignKey("ForecastWeekId")]
    [InverseProperty("ForecastDays")]
    public virtual ForecastWeek? ForecastWeek { get; set; }
}