using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Text.Json;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.OpenApi.Models;
using Swashbuckle.AspNetCore.SwaggerGen;

namespace PowderbookingApi.Extensions;

public static class SwashbuckleExtensions
{
    public static void ConfigureSwagger(this IServiceCollection services)
    {
        services.AddSwaggerGen(c =>
        {
            c.SwaggerDoc("v1",
                new OpenApiInfo
                {
                    Title = "PowderbookingApi", Version = "v1",
                    Description = "Application to show the best hotels with the weather"
                });

            // ref: https://stackoverflow.com/a/54294810/23126405
            c.CustomOperationIds(e =>
                $"{e.ActionDescriptor.RouteValues["controller"]}{e.ActionDescriptor.RouteValues["action"]}");

            // ref: https://learn.microsoft.com/en-us/aspnet/core/tutorials/getting-started-with-swashbuckle?view=aspnetcore-5.0
            // Set the comments path for the Swagger JSON and UI.
            var xmlFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
            var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);
            c.IncludeXmlComments(xmlPath);

            c.SchemaFilter<SnakeCaseSchemaFilter>();
            c.SchemaFilter<NullableToUndefinedFilter>();
        });
    }
}

// ref: https://gist.github.com/GiorgioG/d674892da20369d2a7869cdc1bf7d41b
public class SnakeCasePropertyNamingPolicy : JsonNamingPolicy
{
    public override string ConvertName(string name)
    {
        return name.ToSnakeCase();
    }
}

public class SnakeCaseSchemaFilter : ISchemaFilter
{
    public void Apply(OpenApiSchema schema, SchemaFilterContext context)
    {
        if (schema.Properties == null) return;
        if (schema.Properties.Count == 0) return;

        var keys = schema.Properties.Keys;
        var newProperties = new Dictionary<string, OpenApiSchema>();
        foreach (var key in keys) newProperties[key.ToSnakeCase()] = schema.Properties[key];

        schema.Properties = newProperties;
    }
}

// if the property is not required, it can already be undefined
// in this application, there is no distinction between nullable or undefined
// therefore I'd rather just have undefined
internal class NullableToUndefinedFilter : ISchemaFilter
{
    public void Apply(OpenApiSchema schema, SchemaFilterContext context)
    {
        if (schema.Properties == null) return;
        if (schema.Properties.Count == 0) return;

        foreach (var property in schema.Properties)
            if (property.Value.Nullable && !schema.Required.Contains(property.Key))
                property.Value.Nullable = false;
    }
}