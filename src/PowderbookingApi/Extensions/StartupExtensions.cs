using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using PowderbookingApi.Helpers;
using PowderbookingApi.Repositories;
using PowderbookingApi.Services;

namespace PowderbookingApi.Extensions;

public static class StartupExtensions
{
    public static void ConfigureServices(this IServiceCollection services, IConfiguration config)
    {
        services.AddSingleton(new DbSettings(config));
        services.AddDbContext<PowderbookingContext>();
        services.AddScoped<IResortRepository, ResortRepositoryEf>();
        services.AddScoped<IWeatherRepository, WeatherRepositoryEf>();
        services.AddScoped<IForecastRepository, ForecastRepositoryEf>();
        services.AddScoped<IAvailabilityRepository, AvailabilityRepositoryEf>();
        services.AddScoped<IRankRepository, RankRepositoryEf>();
        services.AddScoped<IResortService, ResortService>();
        services.AddScoped<IRankService, RankService>();
        services.AddScoped<IWeatherService, WeatherService>();
        services.AddScoped<IForecastService, ForecastService>();
        services.AddScoped<IAvailabilityService, AvailabilityService>();
    }
}