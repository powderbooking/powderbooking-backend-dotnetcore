using System.Collections.Generic;
using System.Linq;
using PowderbookingApi.Entities;
using PowderbookingApi.Helpers;

namespace PowderbookingApi.Repositories;

public class WeatherRepositoryEf : IWeatherRepository
{
    private readonly PowderbookingContext _context;

    public WeatherRepositoryEf(PowderbookingContext context)
    {
        _context = context;
    }

    public Weather? GetLatestByResortId(int resortId)
    {
        return _context.Weathers
            .Where(w => w.ResortId == resortId)
            .OrderByDescending(w => w.Created)
            .FirstOrDefault();
    }

    public void Add(Weather weather)
    {
        _context.Weathers.Add(weather);
        _context.SaveChanges();
    }

    public void Add(IEnumerable<Weather> weathers)
    {
        _context.Weathers.AddRange(weathers);
        _context.SaveChanges();
    }
}