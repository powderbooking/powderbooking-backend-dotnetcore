using System.Collections.Generic;
using PowderbookingApi.Entities;

namespace PowderbookingApi.Repositories;

public interface IRankRepository
{
    public Ranking? GetLatest(int type);

    public Ranking? Get(int rankingId);

    public List<ResortRank> GetNext(int fromRank, int rankingId);

    public List<ResortRank> GetPrevious(int fromRank, int rankingId);

    public Ranking Add(int type);

    public void Update(Ranking ranking);

    public void Add(IEnumerable<ResortRank> resortRanks);
}