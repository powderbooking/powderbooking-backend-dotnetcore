using System;
using System.Collections.Generic;
using System.Linq;
using PowderbookingApi.Entities;

namespace PowderbookingApi.Repositories;

public interface IForecastRepository
{
    public ForecastWeek? GetLatest(int resortId);

    public ForecastWeek? GetLatest();

    public ForecastWeek? Get(int resortId, DateOnly date);

    public IQueryable<ForecastWeek> GetAllLatest();

    public IQueryable<ForecastDay> GetPastForecasts(int resortId);

    public void Add(ForecastWeek week);

    public void Add(IEnumerable<ForecastWeek> weeks);
}