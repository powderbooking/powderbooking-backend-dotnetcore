using System.Collections.Generic;
using PowderbookingApi.Entities;

namespace PowderbookingApi.Repositories;

public interface IWeatherRepository
{
    public Weather? GetLatestByResortId(int resortId);

    public void Add(Weather weather);

    public void Add(IEnumerable<Weather> weather);
}