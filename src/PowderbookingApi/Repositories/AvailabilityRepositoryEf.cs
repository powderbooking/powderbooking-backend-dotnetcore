using System;
using System.Collections.Generic;
using System.Linq;
using PowderbookingApi.Entities;
using PowderbookingApi.Helpers;

namespace PowderbookingApi.Repositories;

public class AvailabilityRepositoryEf : IAvailabilityRepository
{
    private readonly PowderbookingContext _context;

    public AvailabilityRepositoryEf(PowderbookingContext context)
    {
        _context = context;
    }

    public IQueryable<Availability> GetCurrentByResortId(int resortId)
    {
        return _context.Availabilities
            .Where(w => w.ResortId == resortId && w.Checkin >= DateOnly.FromDateTime(DateTime.Now));
    }

    public void Add(Availability avail)
    {
        _context.Availabilities.Add(avail);
        _context.SaveChanges();
    }

    public void Add(IEnumerable<Availability> avails)
    {
        _context.Availabilities.AddRange(avails);
        _context.SaveChanges();
    }
}