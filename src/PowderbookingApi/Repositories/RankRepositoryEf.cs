using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using PowderbookingApi.Entities;
using PowderbookingApi.Helpers;

namespace PowderbookingApi.Repositories;

public class RankRepositoryEf : IRankRepository
{
    private readonly int _batchSize = 3;
    private readonly PowderbookingContext _context;

    public RankRepositoryEf(PowderbookingContext context)
    {
        _context = context;
    }

    public Ranking? GetLatest(int type)
    {
        return _context.Rankings
            .Where(w => w.Type == type)
            .OrderByDescending(f => f.Id).FirstOrDefault();
    }

    public Ranking? Get(int rankingId)
    {
        return _context.Rankings.Find(rankingId);
    }

    public List<ResortRank> GetNext(int fromRank, int rankingId)
    {
        return GetBaseQuery(rankingId)
            // ref: https://learn.microsoft.com/en-us/ef/core/querying/pagination#multiple-pagination-keys
            .OrderBy(r => r.Id)
            .Where(r => r.Id > fromRank)
            .Take(_batchSize).ToList();
    }

    public List<ResortRank> GetPrevious(int fromRank, int rankingId)
    {
        var ranks = GetBaseQuery(rankingId)
            // ref: https://learn.microsoft.com/en-us/ef/core/querying/pagination#multiple-pagination-keys
            .OrderByDescending(r => r.Id)
            .Where(r => r.Id < fromRank)
            .Take(_batchSize).ToList();
        ranks.Reverse();
        return ranks;
    }

    public Ranking Add(int type)
    {
        Ranking ranking = new()
        {
            Type = type
        };
        _context.Rankings.Add(ranking);
        _context.SaveChanges();
        return ranking;
    }

    public void Update(Ranking ranking)
    {
        _context.SaveChanges();
    }

    public void Add(IEnumerable<ResortRank> resortRanks)
    {
        _context.ResortRanks.AddRange(resortRanks);
        _context.SaveChanges();
    }

    private IQueryable<ResortRank> GetBaseQuery(int rankingId)
    {
        return _context.ResortRanks
            .Where(r => r.RankingId == rankingId)
            .Include(r => r.Resort)
            .Include(r => r.ForecastWeek)
            .Include(r => r.Ranking);
    }
}