using System;
using System.Collections.Generic;
using System.Linq;
using PowderbookingApi.Entities;
using PowderbookingApi.Helpers;

namespace PowderbookingApi.Repositories;

public class ResortRepositoryEf : IResortRepository
{
    private readonly PowderbookingContext _context;

    public ResortRepositoryEf(PowderbookingContext context)
    {
        _context = context;
    }

    public Resort? GetById(int resortId)
    {
        return _context.Resorts.Find(resortId);
    }

    public IQueryable<Resort> GetByIds(IEnumerable<int> resortIds)
    {
        // ref: https://stackoverflow.com/a/26716193
        var query = _context.Resorts.Join(resortIds, r => r.Id, i => i, (r, i) => r);
        return query;
    }

    public IQueryable<int> GetExistingIds(IEnumerable<int> resortIds)
    {
        // ref: https://stackoverflow.com/a/65257486
        return _context.Resorts.Where(x => resortIds.Contains(x.Id)).Select(x => x.Id);
    }

    public void Add(Resort resort)
    {
        _context.Resorts.Add(resort);
        _context.SaveChanges();
    }

    public void Add(IEnumerable<Resort> resorts)
    {
        _context.Resorts.AddRange(resorts);
        _context.SaveChanges();
    }

    #region scrape

    public IQueryable<Resort> GetResortsToScrapeForWeather()
    {
        var timeDiffCutOff = DateTime.UtcNow.AddHours(-4);
        return _context.Resorts
            .Where(x => x.LastScrapeWeather == null || x.LastScrapeWeather <= timeDiffCutOff);
    }

    public IQueryable<Resort> GetResortsToScrapeForForecast()
    {
        var timeDiffCutOff = DateTime.UtcNow.AddDays(-1);
        return _context.Resorts
            .Where(x => x.LastScrapeForecast == null || x.LastScrapeForecast <= timeDiffCutOff);
    }

    public IQueryable<Resort> GetResortsToScrapeForBooking()
    {
        var timeDiffCutOff = DateTime.UtcNow.AddDays(-1);
        return _context.Resorts
            .Where(x => x.BookingDestId != null)
            .Where(x => x.LastScrapeBooking == null || x.LastScrapeBooking <= timeDiffCutOff);
    }

    public void UpdateLastScrapeForWeather(List<Resort> resorts)
    {
        var now = DateTime.UtcNow;
        resorts.ForEach(resort => resort.LastScrapeWeather = now);
        _context.SaveChanges();
    }

    public void UpdateLastScrapeForForecast(List<Resort> resorts)
    {
        var now = DateTime.UtcNow;
        resorts.ForEach(resort => resort.LastScrapeForecast = now);
        _context.SaveChanges();
    }

    public void UpdateLastScrapeForBooking(List<Resort> resorts)
    {
        var now = DateTime.UtcNow;
        resorts.ForEach(resort => resort.LastScrapeBooking = now);
        _context.SaveChanges();
    }

    #endregion scrape
}