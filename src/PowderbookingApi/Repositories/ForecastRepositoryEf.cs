using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using PowderbookingApi.Entities;
using PowderbookingApi.Helpers;

namespace PowderbookingApi.Repositories;

public class ForecastRepositoryEf : IForecastRepository
{
    private readonly PowderbookingContext _context;

    public ForecastRepositoryEf(PowderbookingContext context)
    {
        _context = context;
    }

    public ForecastWeek? GetLatest(int resortId)
    {
        return _context.ForecastWeeks
            .Include(forecastWeek => forecastWeek.ForecastDays)
            .Where(w => w.ResortId == resortId)
            .OrderByDescending(w => w.Date)
            .FirstOrDefault();
    }

    public ForecastWeek? GetLatest()
    {
        return _context.ForecastWeeks
            .OrderByDescending(f => f.Id)
            .FirstOrDefault();
    }

    public ForecastWeek? Get(int resortId, DateOnly date)
    {
        return _context.ForecastWeeks
            .Include(forecastWeek => forecastWeek.ForecastDays)
            .Where(w => w.ResortId == resortId)
            .Where(w => w.Date == date)
            .FirstOrDefault();
    }

    public IQueryable<ForecastWeek> GetAllLatest()
    {
        var cutoff = DateTime.UtcNow.AddHours(-25);
        return _context.ForecastWeeks
            .Where(x => x.Created >= cutoff)
            .OrderByDescending(x => x.Id)
            .ThenByDescending(x => x.ResortId)
            .GroupBy(x => x.ResortId)
            .Select(x => x.First());
    }

    public IQueryable<ForecastDay> GetPastForecasts(int resortId)
    {
        var todaysDate = DateOnly.FromDateTime(DateTime.Now);

        return _context.ForecastDays
            .Where(w => w.ResortId == resortId && w.Date == todaysDate)
            .OrderByDescending(w => w.Created);
    }

    public void Add(ForecastWeek week)
    {
        _context.ForecastWeeks.Add(week);
        _context.SaveChanges();
    }

    public void Add(IEnumerable<ForecastWeek> weeks)
    {
        _context.ForecastWeeks.AddRange(weeks);
        _context.SaveChanges();
    }
}