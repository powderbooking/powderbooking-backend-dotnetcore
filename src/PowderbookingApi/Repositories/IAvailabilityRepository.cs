using System.Collections.Generic;
using System.Linq;
using PowderbookingApi.Entities;

namespace PowderbookingApi.Repositories;

public interface IAvailabilityRepository
{
    public IQueryable<Availability> GetCurrentByResortId(int resortId);

    public void Add(Availability avail);

    public void Add(IEnumerable<Availability> avails);
}