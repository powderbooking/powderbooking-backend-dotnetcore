using System.Collections.Generic;
using System.Linq;
using PowderbookingApi.Entities;

namespace PowderbookingApi.Repositories;

public interface IResortRepository
{
    public Resort? GetById(int resortId);

    public IQueryable<Resort> GetByIds(IEnumerable<int> resortIds);

    public IQueryable<int> GetExistingIds(IEnumerable<int> resortIds);

    public IQueryable<Resort> GetResortsToScrapeForWeather();

    public IQueryable<Resort> GetResortsToScrapeForForecast();

    public IQueryable<Resort> GetResortsToScrapeForBooking();

    public void UpdateLastScrapeForWeather(List<Resort> resorts);

    public void UpdateLastScrapeForForecast(List<Resort> resorts);

    public void UpdateLastScrapeForBooking(List<Resort> resorts);

    public void Add(Resort resort);

    public void Add(IEnumerable<Resort> resort);
}