using System;
using System.Collections.Generic;
using System.Linq;
using PowderbookingApi.DTOs;
using PowderbookingApi.Entities;
using PowderbookingApi.Repositories;

namespace PowderbookingApi.Services;

public interface IRankService
{
    public RankedBatchDto<OverviewDto> GetFirst(int type);

    public RankedBatchDto<OverviewDto> GetNext(int rank, int rankingId);

    public RankedBatchDto<OverviewDto> GetPrevious(int rank, int rankingId);

    public void RequestRanking();
}

public class RankService(IRankRepository repo, IForecastRepository forecast) : IRankService
{
    // handling the edge case with a mock here to avoid the complexity in the frontend
    private readonly RankedBatchDto<OverviewDto> _default = new()
    {
        Batch = [],
        RankingId = -1
    };

    public RankedBatchDto<OverviewDto> GetFirst(int type)
    {
        var ranking = repo.GetLatest(type);
        if (ranking == null) return _default;

        // mocking a "last ranking" to re-use functionality
        var resortRankings = repo.GetNext(-9999, ranking.Id).ToList();
        if (resortRankings.Count == 0) return _default;

        return ToRankedBatch(resortRankings, ranking);
    }

    public RankedBatchDto<OverviewDto> GetNext(int rank, int rankingId)
    {
        var ranking = repo.Get(rankingId);
        if (ranking == null) return _default;

        var resortRankings = repo.GetNext(rank, rankingId).ToList();
        if (resortRankings.Count == 0) return _default;

        return ToRankedBatch(resortRankings, ranking);
    }

    public RankedBatchDto<OverviewDto> GetPrevious(int rank, int rankingId)
    {
        var ranking = repo.Get(rankingId);
        if (ranking == null) return _default;

        var resortRankings = repo.GetPrevious(rank, rankingId).ToList();
        if (resortRankings.Count == 0) return _default;

        return ToRankedBatch(resortRankings, ranking);
    }

    public void RequestRanking()
    {
        if (!ShouldPerformRanking()) return;

        var ranking = repo.Add(1);
        var forecasts = forecast.GetAllLatest();
        var resortRankings = RankForecasts(forecasts, ranking);
        repo.Add(resortRankings);

        ranking.FirstId = ranking.ResortRanks.First().Id;
        ranking.LastId = ranking.ResortRanks.Last().Id;
        repo.Update(ranking);
    }

    // TODO: unit test
    public static RankedBatchDto<OverviewDto> ToRankedBatch(List<ResortRank> resortRankings, Ranking ranking)
    {
        var firstRankInBatch = resortRankings.First().Id;
        var lastRankInBatch = resortRankings.Last().Id;
        var batch = resortRankings.Select(x => new OverviewDto
        {
            ResortId = x.Resort!.Id,
            Village = x.Resort.Village,
            Lat = x.Resort.Lat,
            Lng = x.Resort.Lng,
            SnowTotalMm = x.ForecastWeek!.SnowTotalMm
        });

        var isFirst = firstRankInBatch <= ranking.FirstId;
        var isLast = lastRankInBatch >= ranking.LastId;
        return new RankedBatchDto<OverviewDto>
        {
            Batch = batch.ToList(),
            RankingId = ranking.Id,
            FirstRank = !isFirst ? firstRankInBatch : null,
            LastRank = !isLast ? lastRankInBatch : null
        };
    }

    // we have a distributed system, so multiple services could ask for a ranking
    // we need to avoid that we are doing a ranking on every whim
    private bool ShouldPerformRanking()
    {
        var latestForecast = forecast.GetLatest();
        // we don't have anything to rank on
        if (latestForecast == null) return false;

        var latestRanking = repo.GetLatest(1);
        if (latestRanking == null) return true;

        return ShouldPerformRanking(latestForecast, latestRanking);
    }

    // TODO: unit test
    private static bool ShouldPerformRanking(ForecastWeek forecast, Ranking ranking)
    {
        // it has new data to make a ranking
        return forecast.Created > ranking.Created
               // the last ranking has also "expired"
               && ranking.Created <= DateTime.UtcNow.AddHours(-1);
    }

    // TODO: unit test
    private static IEnumerable<ResortRank> RankForecasts(IEnumerable<ForecastWeek> forecasts, Ranking ranking)
    {
        var overviews = forecasts.Select(x => new
        {
            x.ResortId,
            x.SnowTotalMm,
            x.Id
        }).ToList();

        var ordered = overviews.OrderByDescending(x => x.SnowTotalMm);

        // TODO: verify that the ordering is maintained within the primary keys
        return ordered.Select(item => new ResortRank
        {
            RankingId = ranking.Id,
            ResortId = item.ResortId,
            ForecastWeekId = item.Id,
            Ranking = ranking
        });
    }
}