using System;
using System.Collections.Generic;
using System.Linq;
using PowderbookingApi.DTOs;
using PowderbookingApi.Helpers.Mappers;
using PowderbookingApi.Repositories;

namespace PowderbookingApi.Services;

public interface IWeatherService
{
    public WeatherDto? GetLatestByResortId(int resortId);

    public IEnumerable<ScrapeDto> GetScrape();

    public void Add(WeatherPostDto weather);

    public void Add(IEnumerable<WeatherPostDto> weathers);
}

public class WeatherService(IWeatherRepository repo, IResortRepository resortRepo) : IWeatherService
{

    public WeatherDto? GetLatestByResortId(int resortId)
    {
        var weather = repo.GetLatestByResortId(resortId);
        if (weather == null) return null;

        return WeatherMapper.ToWeatherDto(weather);
    }

    public IEnumerable<ScrapeDto> GetScrape()
    {
        var resorts = resortRepo.GetResortsToScrapeForWeather();
        if (!resorts.Any()) return [];

        var batch = resorts.Take(200).ToList();
        resortRepo.UpdateLastScrapeForWeather(batch);

        return ResortMapper.ToScrapeDtos(batch);
    }

    public void Add(WeatherPostDto dto)
    {
        var resort = resortRepo.GetById(dto.ResortId);
        if (resort == null) throw new KeyNotFoundException("Resort not found");

        var weather = WeatherMapper.ToWeather(dto);
        repo.Add(weather);
    }

    public void Add(IEnumerable<WeatherPostDto> weathersDtos)
    {
        var possibleResortIds = weathersDtos.Select(w => w.ResortId).Distinct();
        var existingResortIds = resortRepo.GetExistingIds(possibleResortIds);
        if (existingResortIds.Count() != possibleResortIds.Count())
            throw new KeyNotFoundException("Some resorts are not found");

        var weathers = WeatherMapper.ToWeathers(weathersDtos);
        repo.Add(weathers);
    }
}