using System;
using System.Collections.Generic;
using System.Linq;
using PowderbookingApi.DTOs;
using PowderbookingApi.Helpers.Mappers;
using PowderbookingApi.Repositories;

namespace PowderbookingApi.Services;

public interface IForecastService
{
    public ForecastDto? GetLatestByResortId(int resortId);

    public ForecastDto? Get(int resortId, DateOnly date);
    public IEnumerable<ForecastDayDto> GetPastByResortId(int resortId);

    public IEnumerable<ScrapeDto> GetScrape();

    public void Add(ForecastPostDto forecastDto);

    public void Add(IEnumerable<ForecastPostDto> forecastDtos);
}

public class ForecastService(IForecastRepository repo, IResortRepository resortRepo) : IForecastService
{
    public ForecastDto? GetLatestByResortId(int resortId)
    {
        var forecast = repo.GetLatest(resortId);
        if (forecast == null) return null;

        return ForecastMapper.ToForecastDto(forecast);
    }

    public ForecastDto? Get(int resortId, DateOnly date)
    {
        var forecast = repo.Get(resortId, date);
        if (forecast == null) return null;

        return ForecastMapper.ToForecastDto(forecast);
    }

    public IEnumerable<ForecastDayDto> GetPastByResortId(int resortId)
    {
        var forecasts = repo.GetPastForecasts(resortId);
        return ForecastMapper.ToForecastDayDtos(forecasts);
    }

    public IEnumerable<ScrapeDto> GetScrape()
    {
        var resorts = resortRepo.GetResortsToScrapeForForecast();
        if (!resorts.Any()) return [];

        var batch = resorts.Take(200).ToList();
        resortRepo.UpdateLastScrapeForForecast(batch);

        return ResortMapper.ToScrapeDtos(batch);
    }

    public void Add(ForecastPostDto forecastDto)
    {
        var possibleResortId = GetResortId(forecastDto);
        var resort = resortRepo.GetById(possibleResortId);
        if (resort == null) throw new KeyNotFoundException("Resort not found");

        var week = ForecastMapper.ToForecastWeek(forecastDto);
        repo.Add(week);
    }

    public void Add(IEnumerable<ForecastPostDto> forecastDtos)
    {
        var possibleResortIds = forecastDtos.Select(GetResortId).Distinct();
        var existingResortIds = resortRepo.GetExistingIds(possibleResortIds);
        if (existingResortIds.Count() != possibleResortIds.Count())
            throw new KeyNotFoundException("Some resorts are not found");

        var weeks = ForecastMapper.ToForecastWeeks(forecastDtos);
        repo.Add(weeks);
    }

    public static int GetResortId(ForecastPostDto forecastDto)
    {
        var resortIds = forecastDto.ForecastDays
            .Select(e => e.ResortId)
            .Append(forecastDto.ResortId)
            .Distinct();
        if (resortIds.Count() != 1) throw new KeyNotFoundException("Given Resort IDs must match");

        return resortIds.First();
    }
}