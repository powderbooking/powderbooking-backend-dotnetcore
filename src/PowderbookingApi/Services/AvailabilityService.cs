using System;
using System.Collections.Generic;
using System.Linq;
using PowderbookingApi.DTOs;
using PowderbookingApi.Helpers.Mappers;
using PowderbookingApi.Repositories;

namespace PowderbookingApi.Services;

public interface IAvailabilityService
{
    public IEnumerable<AvailabilityDto> GetCurrentByResortId(int resortId);

    public IEnumerable<BookingScrapeDto> GetScrape();

    public void Add(AvailabilityPostDto avail);

    public void Add(IEnumerable<AvailabilityPostDto> avails);
}

public class AvailabilityService(IAvailabilityRepository repo, IResortRepository resortRepo) : IAvailabilityService
{
    public IEnumerable<AvailabilityDto> GetCurrentByResortId(int resortId)
    {
        var avails = repo.GetCurrentByResortId(resortId);
        if (!avails.Any()) return [];

        return AvailabilityMapper.ToAvailabilityDtos(avails);
    }

    public IEnumerable<BookingScrapeDto> GetScrape()
    {
        var resorts = resortRepo.GetResortsToScrapeForBooking();
        if (!resorts.Any()) return [];

        var batch = resorts.Take(200).ToList();
        resortRepo.UpdateLastScrapeForBooking(batch);

        var targets = ResortMapper.ToBookingTargetsDtos(batch);
        return
        [
            new BookingScrapeDto
            {
                Targets = targets,
                Checkin = DateOnly.FromDateTime(DateTime.Today.AddDays(2)),
                Checkout = DateOnly.FromDateTime(DateTime.Today.AddDays(10)),
                Adults = 2,
                Children = 0,
                Rooms = 1
            }
        ];
    }

    public void Add(AvailabilityPostDto dto)
    {
        var resort = resortRepo.GetById(dto.ResortId);
        if (resort == null) throw new KeyNotFoundException("Resort not found");

        var entity = AvailabilityMapper.ToAvailability(dto);
        repo.Add(entity);
    }

    public void Add(IEnumerable<AvailabilityPostDto> dtos)
    {
        var possibleResortIds = dtos.Select(w => w.ResortId).Distinct();
        var existingResortIds = resortRepo.GetExistingIds(possibleResortIds);
        if (existingResortIds.Count() != possibleResortIds.Count())
            throw new KeyNotFoundException("Some resorts are not found");

        var avails = AvailabilityMapper.ToAvailabilities(dtos);
        repo.Add(avails);
    }
}