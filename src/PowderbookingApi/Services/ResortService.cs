using System.Collections.Generic;
using PowderbookingApi.DTOs;
using PowderbookingApi.Exceptions;
using PowderbookingApi.Helpers.Mappers;
using PowderbookingApi.Repositories;

namespace PowderbookingApi.Services;

public interface IResortService
{
    public ResortDto? GetById(int resortId);

    public void Add(ResortPostDto dto);

    public void Add(ResortPostDto dto, int resortId);
}

public class ResortService(IResortRepository repo) : IResortService
{

    public ResortDto? GetById(int resortId)
    {
        var resort = repo.GetById(resortId) ?? throw new KeyNotFoundException("Resort not found");
        return ResortMapper.ToResortDto(resort);
    }

    public void Add(ResortPostDto dto)
    {
        repo.Add(ResortMapper.ToResort(dto));
    }

    public void Add(ResortPostDto dto, int resortId)
    {
        var knownResort = repo.GetById(resortId);
        if (knownResort != null) throw new BadRequestException("A resort with given ID is already known");
        var resort = ResortMapper.ToResort(dto);
        resort.Id = resortId;
        repo.Add(resort);
    }
}