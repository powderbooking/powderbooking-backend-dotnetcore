﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using PowderbookingApi.DTOs;
using PowderbookingApi.Services;

namespace PowderbookingApi.Controllers;

[ApiController]
[Route("[controller]")]
[Produces("application/json")]
public class ResortController : ControllerBase
{
    private readonly IResortService _svc;

    public ResortController(IResortService svc)
    {
        _svc = svc;
    }

    /// <summary>
    ///     Get overview details given its identifier
    /// </summary>
    /// <param name="resortId">The resort identifier</param>
    /// <returns>Resort</returns>
    [HttpGet("{resortId:int}")]
    [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(ResortDto))]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    public IActionResult Get(int resortId)
    {
        var resort = _svc.GetById(resortId);

        return Ok(resort);
    }

    /// <summary>
    ///     Add a new resort
    /// </summary>
    [HttpPost]
    [ProducesResponseType(StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    public IActionResult Post(ResortPostDto dto)
    {
        _svc.Add(dto);
        return Ok();
    }

    /// <summary>
    ///     Add a new resort, with a specific resort ID
    /// </summary>
    [HttpPost("{resortId:int}")]
    [ProducesResponseType(StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    public IActionResult PostWithId(int resortId, ResortPostDto dto)
    {
        _svc.Add(dto, resortId);
        return Ok();
    }
}