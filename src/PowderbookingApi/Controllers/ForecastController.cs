﻿using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using PowderbookingApi.DTOs;
using PowderbookingApi.Services;

namespace PowderbookingApi.Controllers;

[ApiController]
[Route("[controller]")]
[Produces("application/json")]
public class ForecastController : ControllerBase
{
    private readonly IForecastService _svc;

    public ForecastController(IForecastService svc)
    {
        _svc = svc;
    }

    /// <summary>
    ///     Get the current forecast report from today for the given overview identifier
    /// </summary>
    /// <param name="resortId">The resort identifier</param>
    /// <returns>the most recent forecast</returns>
    [HttpGet("current/{resortId:int}")]
    [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(ForecastDto))]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    public IActionResult GetCurrent(int resortId)
    {
        var forecast = _svc.GetLatestByResortId(resortId);
        if (forecast == null) return NotFound();

        return Ok(forecast);
    }

    /// <summary>
    ///     Get the forecast report from a given day for the given overview identifier
    /// </summary>
    /// <param name="resortId">The resort identifier</param>
    /// <param name="date">The date of the forecast (in yyyy-mm-dd format)</param>
    /// <returns>the forecast, if present</returns>
    [HttpGet("{resortId:int}/{date:datetime}")]
    [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(ForecastDto))]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    public IActionResult Get(int resortId, DateTime date)
    {
        var forecast = _svc.Get(resortId, DateOnly.FromDateTime(date));
        if (forecast == null) return NotFound();

        return Ok(forecast);
    }

    /// <summary>
    ///     Get the past forecast reports of today for the given overview identifier
    /// </summary>
    /// <param name="resortId">The resort identifier</param>
    /// <returns>List of forecasts</returns>
    [HttpGet("past/{resortId:int}")]
    [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(IEnumerable<ForecastDayDto>))]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    public IActionResult GetPast(int resortId)
    {
        var forecasts = _svc.GetPastByResortId(resortId);
        if (forecasts == null) return NotFound();

        return Ok(forecasts);
    }

    /// <summary>
    ///     Get resorts that need to be scraped for forecast
    /// </summary>
    /// <returns>A list of resorts to scrape</returns>
    [HttpGet("scrape")]
    [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(IEnumerable<ScrapeDto>))]
    public IActionResult Scrape()
    {
        var scrape = _svc.GetScrape();
        return Ok(scrape);
    }

    /// <summary>
    ///     Insert a new report of a Forecast
    /// </summary>
    [HttpPost]
    [ProducesResponseType(StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    public IActionResult Post(ForecastPostDto forecastDto)
    {
        _svc.Add(forecastDto);
        return Ok();
    }

    /// <summary>
    ///     Insert an array of new reports of a Forecast
    /// </summary>
    [HttpPost("bulk")]
    [ProducesResponseType(StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    public IActionResult PostBulk(IEnumerable<ForecastPostDto> forecastDtos)
    {
        _svc.Add(forecastDtos);
        return Ok();
    }
}