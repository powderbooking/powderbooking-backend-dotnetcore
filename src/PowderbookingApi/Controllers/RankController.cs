﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using PowderbookingApi.DTOs;
using PowderbookingApi.Services;

namespace PowderbookingApi.Controllers;

[ApiController]
[Route("[controller]")]
[Produces("application/json")]
public class RankController : ControllerBase
{
    private readonly IRankService _svc;

    public RankController(IRankService svc)
    {
        _svc = svc;
    }

    /// <summary>
    ///     Return the first batch
    /// </summary>
    /// <returns>Batch of Overviews</returns>
    [HttpGet("forecast")]
    public RankedBatchDto<OverviewDto> Get()
    {
        return _svc.GetFirst(1);
    }

    /// <summary>
    ///     Return the next batch
    /// </summary>
    /// <returns>Batch of Overviews</returns>
    [HttpGet("next")]
    public RankedBatchDto<OverviewDto> GetNext([FromQuery] int rank, [FromQuery] int rankingId)
    {
        return _svc.GetNext(rank, rankingId);
    }

    /// <summary>
    ///     Return the previous batch
    /// </summary>
    /// <returns>Batch of Overviews</returns>
    [HttpGet("previous")]
    public RankedBatchDto<OverviewDto> GetPrevious([FromQuery] int rank, [FromQuery] int rankingId)
    {
        return _svc.GetPrevious(rank, rankingId);
    }

    /// <summary>
    ///     Request a ranking
    /// </summary>
    [HttpPost]
    [ProducesResponseType(StatusCodes.Status202Accepted)]
    public IActionResult PostRequest()
    {
        _svc.RequestRanking();
        return Accepted();
    }
}