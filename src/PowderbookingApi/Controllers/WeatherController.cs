﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using PowderbookingApi.DTOs;
using PowderbookingApi.Services;

namespace PowderbookingApi.Controllers;

[ApiController]
[Route("[controller]")]
[Produces("application/json")]
public class WeatherController : ControllerBase
{
    private readonly IWeatherService _svc;

    public WeatherController(IWeatherService svc)
    {
        _svc = svc;
    }

    /// <summary>
    ///     Get the latest weather report for the given overview identifier
    /// </summary>
    /// <param name="resortId">The resort identifier</param>
    /// <returns>Weather</returns>
    [HttpGet("{resortId:int}")]
    [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(WeatherDto))]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    public IActionResult Get(int resortId)
    {
        var weather = _svc.GetLatestByResortId(resortId);
        if (weather == null) return NotFound();

        return Ok(weather);
    }

    /// <summary>
    ///     Get resorts that need to be scraped for weather
    /// </summary>
    /// <returns>A list of resorts to scrape</returns>
    [HttpGet("scrape")]
    [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(IEnumerable<ScrapeDto>))]
    public IActionResult Scrape()
    {
        var scrape = _svc.GetScrape();
        return Ok(scrape);
    }

    /// <summary>
    ///     Insert a new Weather report
    /// </summary>
    [HttpPost]
    [ProducesResponseType(StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    public IActionResult Post(WeatherPostDto weatherDto)
    {
        _svc.Add(weatherDto);
        return Ok();
    }

    /// <summary>
    ///     Insert an array of new Weather reports
    /// </summary>
    [HttpPost("bulk")]
    [ProducesResponseType(StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    public IActionResult PostBulk(WeatherPostDto[] weatherDtos)
    {
        _svc.Add(weatherDtos);
        return Ok();
    }
}