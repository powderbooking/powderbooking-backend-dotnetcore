﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using PowderbookingApi.DTOs;
using PowderbookingApi.Services;

namespace PowderbookingApi.Controllers;

[ApiController]
[Route("[controller]")]
[Produces("application/json")]
public class AvailabilityController : ControllerBase
{
    private readonly IAvailabilityService _svc;

    public AvailabilityController(IAvailabilityService svc)
    {
        _svc = svc;
    }

    /// <summary>
    ///     Get the currently relevant availabilities of the given overview identifier
    /// </summary>
    /// <param name="resortId">The resort identifier</param>
    /// <returns>A list of availabilities</returns>
    [HttpGet("{resortId:int}")]
    [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(IEnumerable<AvailabilityDto>))]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    public IActionResult Get(int resortId)
    {
        var entity = _svc.GetCurrentByResortId(resortId);
        if (entity == null) return NotFound();

        return Ok(entity);
    }

    /// <summary>
    ///     Get resorts that need to be scraped for availability
    /// </summary>
    /// <returns>A list of resorts to scrape</returns>
    [HttpGet("scrape")]
    [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(IEnumerable<BookingScrapeDto>))]
    public IActionResult Scrape()
    {
        var scrape = _svc.GetScrape();
        return Ok(scrape);
    }

    /// <summary>
    ///     Insert a new Availability report
    /// </summary>
    [HttpPost]
    [ProducesResponseType(StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    public IActionResult Post(AvailabilityPostDto dto)
    {
        _svc.Add(dto);
        return Ok();
    }

    /// <summary>
    ///     Insert an array of new Availability reports
    /// </summary>
    [HttpPost("bulk")]
    [ProducesResponseType(StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    public IActionResult PostBulk(AvailabilityPostDto[] dtos)
    {
        _svc.Add(dtos);
        return Ok();
    }
}