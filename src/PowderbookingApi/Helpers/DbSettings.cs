using Microsoft.Extensions.Configuration;

namespace PowderbookingApi.Helpers;

public class DbSettings
{
    private readonly string _database;
    private readonly string _host;
    private readonly string _password;
    private readonly string _port;
    private readonly string _user;

    public DbSettings(IConfiguration configuration)
    {
        _host = configuration.GetValue<string>("POSTGRESQL_HOST")!;
        _port = configuration.GetValue<string>("POSTGRESQL_PORT")!;
        _database = configuration.GetValue<string>("POSTGRESQL_DATABASE")!;
        _user = configuration.GetValue<string>("POSTGRESQL_USER")!;
        _password = configuration.GetValue<string>("POSTGRESQL_PASSWORD")!;
    }

    public string ConnectionString()
    {
        return $"Host={_host}; Port={_port}; Database={_database}; Username={_user}; Password={_password};";
    }
}