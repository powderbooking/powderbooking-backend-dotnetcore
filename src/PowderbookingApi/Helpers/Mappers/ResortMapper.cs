using System.Collections.Generic;
using PowderbookingApi.DTOs;
using PowderbookingApi.Entities;
using Riok.Mapperly.Abstractions;

namespace PowderbookingApi.Helpers.Mappers;

[Mapper]
public static partial class ResortMapper
{
    public static partial ResortDto ToResortDto(Resort entity);

    public static partial Resort ToResort(ResortDto dto);

    public static partial Resort ToResort(ResortPostDto dto);

    public static partial ScrapeDto ToScrapeDto(Resort entity);

    public static partial List<ScrapeDto> ToScrapeDtos(IEnumerable<Resort> entities);

    public static partial BookingTargetsDto ToBookingTargetsDto(Resort entity);

    public static partial List<BookingTargetsDto> ToBookingTargetsDtos(IEnumerable<Resort> entities);
}