using System;
using System.Collections.Generic;
using PowderbookingApi.DTOs;
using PowderbookingApi.Entities;
using Riok.Mapperly.Abstractions;

namespace PowderbookingApi.Helpers.Mappers;

[Mapper]
public static partial class ForecastMapper
{
    public static partial ForecastDayDto ToForecastDayDto(ForecastDay entity);

    public static partial List<ForecastDayDto> ToForecastDayDtos(IEnumerable<ForecastDay> entities);

    public static partial ForecastDay ToForecastDay(ForecastDayDto dto);

    // these are a private methods because we are mocking the timepoint here
    // ForecastDay should only be created from a ForecastPostDto (in which the timepoint is properly set)
    // we do want to allow mapperly to map it inside this mapper
    // alternatively, we could make timepoint non-required
    [MapProperty(nameof(ForecastDayPostDto.Date), nameof(ForecastDay.Timepoint), Use = nameof(MapMockedTimepoint))]
    private static partial ForecastDay ToForecastDay(ForecastDayPostDto dto);
    private static int MapMockedTimepoint(DateOnly d) => d.DayNumber;
    private static partial List<ForecastDay> ToForecastDays(IEnumerable<ForecastDayPostDto> dtos);

    public static partial ForecastDto ToForecastDto(ForecastWeek entity);

    // ref: https://mapperly.riok.app/docs/configuration/before-after-map/
    [UserMapping(Default = true)]
    public static ForecastWeek ToForecastWeek(ForecastPostDto dto)
    {
        var week = ToForecastWeekInner(dto);
        var weekDayNumber = week.Date.DayNumber;
        foreach (var day in week.ForecastDays ?? []) day.Timepoint = day.Date.DayNumber - weekDayNumber;
        return week;
    }
    private static partial ForecastWeek ToForecastWeekInner(ForecastPostDto dto);

    public static partial List<ForecastWeek> ToForecastWeeks(IEnumerable<ForecastPostDto> dtos);
}