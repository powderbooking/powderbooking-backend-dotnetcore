using System;
using System.Collections.Generic;
using PowderbookingApi.DTOs;
using PowderbookingApi.Entities;
using Riok.Mapperly.Abstractions;

namespace PowderbookingApi.Helpers.Mappers;

[Mapper]
public static partial class WeatherMapper
{
    public static partial WeatherDto ToWeatherDto(Weather entity);

    public static partial Weather ToWeather(WeatherDto dto);

    [MapProperty(nameof(WeatherPostDto.Dt), nameof(Weather.Date))]
    [MapProperty(nameof(WeatherPostDto.Dt), nameof(Weather.Timepoint), Use = nameof(MapTimepoint))]
    public static partial Weather ToWeather(WeatherPostDto entity);

    private static int MapTimepoint(DateTime dt) => (int)Math.Round(dt.Hour / 3d, 0, MidpointRounding.ToZero);

    public static partial List<Weather> ToWeathers(IEnumerable<WeatherPostDto> entities);
}