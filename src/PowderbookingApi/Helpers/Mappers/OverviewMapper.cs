using PowderbookingApi.DTOs;
using PowderbookingApi.Entities;
using Riok.Mapperly.Abstractions;

namespace PowderbookingApi.Helpers.Mappers;

[Mapper]
public static partial class OverviewMapper
{
    [MapNestedProperties(nameof(ForecastWeek.Resort))]
    public static partial OverviewDto ToOverviewDto(ForecastWeek entity);
}