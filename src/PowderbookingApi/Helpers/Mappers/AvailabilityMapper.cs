using System.Collections.Generic;
using PowderbookingApi.DTOs;
using PowderbookingApi.Entities;
using Riok.Mapperly.Abstractions;

namespace PowderbookingApi.Helpers.Mappers;

[Mapper]
public static partial class AvailabilityMapper
{
    public static partial AvailabilityDto ToAvailabilityDto(Availability entity);

    public static partial List<AvailabilityDto> ToAvailabilityDtos(IEnumerable<Availability> entities);

    public static partial Availability ToAvailability(AvailabilityPostDto dto);

    public static partial List<Availability> ToAvailabilities(IEnumerable<AvailabilityPostDto> dtos);
}