﻿using Microsoft.EntityFrameworkCore;
using PowderbookingApi.Entities;

namespace PowderbookingApi.Helpers;

public partial class PowderbookingContext : DbContext
{
    private readonly DbSettings _dbSettings;

    public PowderbookingContext(DbSettings settings)
    {
        _dbSettings = settings;
    }

    // ref: https://learn.microsoft.com/en-us/ef/core/miscellaneous/nullable-reference-types#dbcontext-and-dbset
    public virtual DbSet<ForecastDay> ForecastDays => Set<ForecastDay>();

    public virtual DbSet<ForecastWeek> ForecastWeeks => Set<ForecastWeek>();

    public virtual DbSet<Resort> Resorts => Set<Resort>();

    public virtual DbSet<Weather> Weathers => Set<Weather>();

    public virtual DbSet<Availability> Availabilities => Set<Availability>();

    public virtual DbSet<Ranking> Rankings => Set<Ranking>();

    public virtual DbSet<ResortRank> ResortRanks => Set<ResortRank>();

    protected override void OnConfiguring(DbContextOptionsBuilder options)
    {
        options.UseNpgsql(_dbSettings.ConnectionString());
    }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        modelBuilder.Entity<ForecastDay>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("forecast_day_pkey");
            entity.Property(e => e.Created).HasDefaultValueSql("now()").HasComment("created timestamp");
            entity.HasOne(d => d.ForecastWeek).WithMany(p => p.ForecastDays)
                .OnDelete(DeleteBehavior.Cascade)
                .HasConstraintName("forecast_day_forecast_week_id_fkey");
            entity.HasOne(d => d.Resort).WithMany(p => p.ForecastDays)
                .OnDelete(DeleteBehavior.Cascade)
                .HasConstraintName("forecast_day_resort_id_fkey");
        });

        modelBuilder.Entity<ForecastWeek>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("forecast_week_pkey");
            entity.Property(e => e.Created).HasDefaultValueSql("now()").HasComment("created timestamp");
            entity.HasOne(d => d.Resort).WithMany(p => p.ForecastWeeks)
                .OnDelete(DeleteBehavior.Cascade)
                .HasConstraintName("forecast_week_resort_id_fkey");
        });

        modelBuilder.Entity<Resort>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("resort_pkey");
            // We are read heavy and only updates (no inserts)
            // I'd think it would be beneficial to add an index
            // However, I don't know how this interacts with EF core updates
            // TODO: Leaving it without for now, lets see what happens
            // entity.HasIndex(e => e.LastScrapeForecast);
            // entity.HasIndex(e => e.LastScrapeWeather);
            // entity.HasIndex(e => e.LastScrapeBooking);
        });

        modelBuilder.Entity<Weather>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("weather_pkey");
            entity.Property(e => e.Created).HasDefaultValueSql("now()").HasComment("created timestamp");
            entity.HasOne(d => d.Resort).WithMany(p => p.Weathers)
                .OnDelete(DeleteBehavior.Cascade)
                .HasConstraintName("weather_resort_id_fkey");
        });

        modelBuilder.Entity<Availability>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("availability_pkey");
            entity.Property(e => e.Created).HasDefaultValueSql("now()").HasComment("created timestamp");
            entity.HasOne(d => d.Resort).WithMany(p => p.Availabilities)
                .OnDelete(DeleteBehavior.Cascade)
                .HasConstraintName("availability_resort_id_fkey");
        });

        modelBuilder.Entity<Ranking>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("ranking_pkey");
            entity.HasIndex(e => new { e.Type, e.Created });
            entity.Property(e => e.Created).HasDefaultValueSql("now()").HasComment("created timestamp");
        });

        modelBuilder.Entity<ResortRank>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("resort_rank_pkey");
            entity.HasOne(d => d.Resort).WithMany(p => p.ResortRanks)
                .OnDelete(DeleteBehavior.Cascade)
                .HasConstraintName("resort_rank_resort_id_fkey");
            entity.HasOne(d => d.Ranking).WithMany(p => p.ResortRanks)
                .OnDelete(DeleteBehavior.Cascade)
                .HasConstraintName("resort_rank_ranking_id_fkey");
            entity.HasOne(d => d.ForecastWeek).WithMany(p => p.ResortRanks)
                .OnDelete(DeleteBehavior.Cascade)
                .HasConstraintName("resort_rank_forecast_week_id_fkey");
            entity.HasIndex(e => new { e.ResortId, e.RankingId });
        });

        OnModelCreatingPartial(modelBuilder);
    }

    partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
}