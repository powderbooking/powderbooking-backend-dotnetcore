﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace PowderbookingApi.Migrations
{
    /// <inheritdoc />
    public partial class AddLastScrapeTimeStamps : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<DateTime>(
                name: "last_scrape_booking",
                table: "resort",
                type: "timestamp with time zone",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "last_scrape_forecast",
                table: "resort",
                type: "timestamp with time zone",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "last_scrape_weather",
                table: "resort",
                type: "timestamp with time zone",
                nullable: true);
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "last_scrape_booking",
                table: "resort");

            migrationBuilder.DropColumn(
                name: "last_scrape_forecast",
                table: "resort");

            migrationBuilder.DropColumn(
                name: "last_scrape_weather",
                table: "resort");
        }
    }
}
