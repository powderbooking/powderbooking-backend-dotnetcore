﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

#nullable disable

namespace PowderbookingApi.Migrations
{
    /// <inheritdoc />
    public partial class AddAvailabilities : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "booking_dest_id",
                table: "resort",
                type: "character varying",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "availability",
                columns: table => new
                {
                    id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    created = table.Column<DateTime>(type: "timestamp with time zone", nullable: true, defaultValueSql: "now()", comment: "created timestamp"),
                    resort_id = table.Column<int>(type: "integer", nullable: false),
                    name = table.Column<string>(type: "text", nullable: false),
                    url = table.Column<string>(type: "text", nullable: false),
                    rating = table.Column<string>(type: "text", nullable: true),
                    checkin = table.Column<DateOnly>(type: "date", nullable: false),
                    checkout = table.Column<DateOnly>(type: "date", nullable: false),
                    price = table.Column<int>(type: "integer", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("availability_pkey", x => x.id);
                    table.ForeignKey(
                        name: "availability_resort_id_fkey",
                        column: x => x.resort_id,
                        principalTable: "resort",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_availability_resort_id",
                table: "availability",
                column: "resort_id");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "availability");

            migrationBuilder.DropColumn(
                name: "booking_dest_id",
                table: "resort");
        }
    }
}
