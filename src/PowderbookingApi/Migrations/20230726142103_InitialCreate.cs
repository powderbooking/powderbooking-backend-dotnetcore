﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

#nullable disable

namespace PowderbookingApi.Migrations
{
    /// <inheritdoc />
    public partial class InitialCreate : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "resort",
                columns: table => new
                {
                    id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    continent = table.Column<string>(type: "character varying", nullable: false),
                    country = table.Column<string>(type: "character varying", nullable: false),
                    village = table.Column<string>(type: "character varying", nullable: false),
                    lat = table.Column<double>(type: "double precision", nullable: false),
                    lng = table.Column<double>(type: "double precision", nullable: false),
                    altitude_min_m = table.Column<int>(type: "integer", nullable: true),
                    altitude_max_m = table.Column<int>(type: "integer", nullable: true),
                    lifts = table.Column<int>(type: "integer", nullable: true),
                    slopes_total_km = table.Column<int>(type: "integer", nullable: true),
                    slopes_blue_km = table.Column<int>(type: "integer", nullable: true),
                    slopes_red_km = table.Column<int>(type: "integer", nullable: true),
                    slopes_black_km = table.Column<int>(type: "integer", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("resort_pkey", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "forecast_week",
                columns: table => new
                {
                    id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    created = table.Column<DateTime>(type: "timestamp with time zone", nullable: true, defaultValueSql: "now()", comment: "created timestamp"),
                    resort_id = table.Column<int>(type: "integer", nullable: false),
                    date = table.Column<DateOnly>(type: "date", nullable: true),
                    rain_total_mm = table.Column<double>(type: "double precision", nullable: false),
                    snow_total_mm = table.Column<double>(type: "double precision", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("forecast_week_pkey", x => x.id);
                    table.ForeignKey(
                        name: "forecast_week_resort_id_fkey",
                        column: x => x.resort_id,
                        principalTable: "resort",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "weather",
                columns: table => new
                {
                    id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    created = table.Column<DateTime>(type: "timestamp with time zone", nullable: true, defaultValueSql: "now()", comment: "created timestamp"),
                    resort_id = table.Column<int>(type: "integer", nullable: false),
                    dt = table.Column<DateTime>(type: "timestamp with time zone", nullable: false),
                    date = table.Column<DateOnly>(type: "date", nullable: false),
                    timepoint = table.Column<int>(type: "integer", nullable: false),
                    temperature_c = table.Column<double>(type: "double precision", nullable: true),
                    wind_speed_kmh = table.Column<double>(type: "double precision", nullable: true),
                    wind_direction_deg = table.Column<double>(type: "double precision", nullable: true),
                    visibility_km = table.Column<double>(type: "double precision", nullable: true),
                    clouds_pct = table.Column<double>(type: "double precision", nullable: true),
                    snow_3h_mm = table.Column<double>(type: "double precision", nullable: true),
                    rain_3h_mm = table.Column<double>(type: "double precision", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("weather_pkey", x => x.id);
                    table.ForeignKey(
                        name: "weather_resort_id_fkey",
                        column: x => x.resort_id,
                        principalTable: "resort",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "forecast_day",
                columns: table => new
                {
                    id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    created = table.Column<DateTime>(type: "timestamp with time zone", nullable: true, defaultValueSql: "now()", comment: "created timestamp"),
                    resort_id = table.Column<int>(type: "integer", nullable: false),
                    forecast_week_id = table.Column<int>(type: "integer", nullable: false),
                    date = table.Column<DateOnly>(type: "date", nullable: false),
                    timepoint = table.Column<int>(type: "integer", nullable: false),
                    temperature_max_c = table.Column<double>(type: "double precision", nullable: true),
                    temperature_min_c = table.Column<double>(type: "double precision", nullable: true),
                    rain_total_mm = table.Column<double>(type: "double precision", nullable: true),
                    snow_total_mm = table.Column<double>(type: "double precision", nullable: true),
                    prob_precip_pct = table.Column<double>(type: "double precision", nullable: true),
                    wind_speed_max_kmh = table.Column<double>(type: "double precision", nullable: true),
                    windgst_max_kmh = table.Column<double>(type: "double precision", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("forecast_day_pkey", x => x.id);
                    table.ForeignKey(
                        name: "forecast_day_forecast_week_id_fkey",
                        column: x => x.forecast_week_id,
                        principalTable: "forecast_week",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "forecast_day_resort_id_fkey",
                        column: x => x.resort_id,
                        principalTable: "resort",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "forecast_day_date_timepoint_resort_id_key",
                table: "forecast_day",
                columns: new[] { "date", "timepoint", "resort_id" },
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_forecast_day_forecast_week_id",
                table: "forecast_day",
                column: "forecast_week_id");

            migrationBuilder.CreateIndex(
                name: "IX_forecast_day_resort_id",
                table: "forecast_day",
                column: "resort_id");

            migrationBuilder.CreateIndex(
                name: "forecast_week_date_resort_id_key",
                table: "forecast_week",
                columns: new[] { "date", "resort_id" },
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_forecast_week_resort_id",
                table: "forecast_week",
                column: "resort_id");

            migrationBuilder.CreateIndex(
                name: "resort_village_country_key",
                table: "resort",
                columns: new[] { "village", "country" },
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_weather_resort_id",
                table: "weather",
                column: "resort_id");

            migrationBuilder.CreateIndex(
                name: "weather_date_timepoint_resort_id_key",
                table: "weather",
                columns: new[] { "date", "timepoint", "resort_id" },
                unique: true);
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "forecast_day");

            migrationBuilder.DropTable(
                name: "weather");

            migrationBuilder.DropTable(
                name: "forecast_week");

            migrationBuilder.DropTable(
                name: "resort");
        }
    }
}
