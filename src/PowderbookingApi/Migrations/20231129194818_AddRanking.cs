﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

#nullable disable

namespace PowderbookingApi.Migrations
{
    /// <inheritdoc />
    public partial class AddRanking : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "ranking",
                columns: table => new
                {
                    id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    created = table.Column<DateTime>(type: "timestamp with time zone", nullable: true, defaultValueSql: "now()", comment: "created timestamp"),
                    type = table.Column<int>(type: "integer", nullable: false),
                    first_id = table.Column<int>(type: "integer", nullable: false),
                    last_id = table.Column<int>(type: "integer", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("ranking_pkey", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "resort_rank",
                columns: table => new
                {
                    id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    resort_id = table.Column<int>(type: "integer", nullable: false),
                    ranking_id = table.Column<int>(type: "integer", nullable: false),
                    forecast_week_id = table.Column<int>(type: "integer", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("resort_rank_pkey", x => x.id);
                    table.ForeignKey(
                        name: "resort_rank_forecast_week_id_fkey",
                        column: x => x.forecast_week_id,
                        principalTable: "forecast_week",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "resort_rank_ranking_id_fkey",
                        column: x => x.ranking_id,
                        principalTable: "ranking",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "resort_rank_resort_id_fkey",
                        column: x => x.resort_id,
                        principalTable: "resort",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_ranking_type_created",
                table: "ranking",
                columns: new[] { "type", "created" });

            migrationBuilder.CreateIndex(
                name: "IX_resort_rank_forecast_week_id",
                table: "resort_rank",
                column: "forecast_week_id");

            migrationBuilder.CreateIndex(
                name: "IX_resort_rank_ranking_id",
                table: "resort_rank",
                column: "ranking_id");

            migrationBuilder.CreateIndex(
                name: "IX_resort_rank_resort_id_ranking_id",
                table: "resort_rank",
                columns: new[] { "resort_id", "ranking_id" });

            migrationBuilder.CreateIndex(
                name: "resort_ranking_key",
                table: "resort_rank",
                columns: new[] { "resort_id", "ranking_id" },
                unique: true);
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "resort_rank");

            migrationBuilder.DropTable(
                name: "ranking");
        }
    }
}
